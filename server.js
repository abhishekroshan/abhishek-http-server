const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const { error } = require("console");

const PORT = 3000;

const server = http.createServer((req, res) => {
  // Splitting the URL into an array for easier parsing

  const urlArray = req.url.split("/");

  let delay;
  let statusCode;

  // Parsing the URL to get delay or statusCode

  if (urlArray[1] === "status") {
    statusCode = Number(urlArray[2]);
  } else if (urlArray[1] === "delay") {
    delay = Number(urlArray[2]);
  }

  if (req.url == "/" && req.method == "GET") {
    // Homepage

    res.writeHead(200, { "Content-type": "text/html" });

    res.end("<h1>This is Homepage, Try other routes</h1>");
  } else if (req.url == "/html" && req.method == "GET") {
    // Reading and serving HTML file

    fs.readFile("./index.html", "utf-8", (err, data) => {
      if (err) {
        throw new error("Error occured in reading html file");
      }
      res.writeHead(200, { "Content-type": "text/html" });

      res.end(data);
    });
  } else if (req.url == "/json" && req.method == "GET") {
    // Reading JSON file

    res.writeHead(200, { "Content-type": "text/json" });

    fs.readFile("./file.json", "utf-8", (err, data) => {
      if (err) {
        throw new error("Error occured in reading json file");
      }
      res.end(data);
    });
  } else if (req.url == "/uuid" && req.method == "GET") {
    // Generating and using the UUID

    const generateUUID = uuidv4();

    const random_uuid = {
      uuid: generateUUID,
    };
    res.writeHead(200, { "Content-type": "text/json" });

    res.end(JSON.stringify(random_uuid));
  } else if (req.url == `/status/${statusCode}` && req.method == "GET") {
    // Handling status code routes

    if (statusCode < 200) {
      // For informational status codes (1xx), handling as 200

      res.writeHead(200, { "Content-type": "text/html" });

      res.end(
        "<h1> This is 100 or 1xx status code, used for information purpose. But in the writeHead we have passed 200 because with 100 we are getting redirected to error 404 page</h1>"
      );
    } else {
      // For other status codes

      res.writeHead(statusCode, { "Content-type": "text/html" });

      res.end(`<h1> This is ${statusCode} status code. </h1>`);
    }
  } else if (req.url == `/delay/${delay}` && req.method == "GET") {
    // Handling delay routes

    setTimeout(() => {
      res.writeHead(200, { "Content-type": "text/html" });

      res.end(
        `<h1>This text has appeared after a delay of ${delay} seconds.</h1>`
      );
    }, delay * 1000);
  } else {
    // Default 404 response for unknown routes

    res.writeHead(404, { "Content-type": "text/html" });

    res.end("<h1>Error 404. Page not found</h1>");
  }
});

server.listen(PORT);
console.log(`Server started on Port ${PORT}`);
